import { Box, Button, CircularProgress, TextField } from '@mui/material';
import { styled } from '@mui/system';
import { useParams } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';
import { IPerson } from '@lib/models/person';
import { useContactEdit } from '@hooks/contacts/useContactEdit';
import { useEffect } from 'react';

const FormWrapper = styled('form')(({ theme }) => ({
  padding: theme.spacing(2)
}));

const InputsWrapper = styled(Box)(({ theme }) => ({
  display: 'grid',
  gap: theme.spacing(2),
  marginBottom: theme.spacing(2)
}));

export function ContactEditPage() {
  const { id } = useParams();

  const { contact, update } = useContactEdit(id);

  const {
    handleSubmit,
    control,
    setValue,
    formState: { isSubmitting }
  } = useForm<IPerson>({
    defaultValues: {
      firstName: '',
      lastName: '',
      email: ''
    }
  });

  const handleEditContact = async (data: IPerson) => {
    console.log('sending data...');
    await update(data);
  };

  useEffect(() => {
    if (contact) {
      setValue('id', contact.id);
      setValue('firstName', contact.firstName);
      setValue('lastName', contact.lastName);
      setValue('email', contact.email);
    }
  }, [contact]);

  return (
    <FormWrapper onSubmit={handleSubmit(handleEditContact)}>
      <InputsWrapper>
        <Controller
          control={control}
          name="firstName"
          render={({ field, fieldState: { error } }) => (
            <TextField
              {...field}
              label="First name"
              error={!!error}
              sx={{ gridColumn: 'span 1' }}
            />
          )}
        />
        <Controller
          control={control}
          name="lastName"
          render={({ field, fieldState: { error } }) => (
            <TextField
              {...field}
              label="Last name"
              error={!!error}
              sx={{ gridColumn: 'span 1' }}
            />
          )}
        />
        <Controller
          control={control}
          name="email"
          render={({ field, fieldState: { error } }) => (
            <TextField
              {...field}
              label="Email"
              error={!!error}
              sx={{ gridColumn: 'span 2' }}
            />
          )}
        />
      </InputsWrapper>
      <Button type="submit" disabled={isSubmitting} variant="contained">
        Save
      </Button>
    </FormWrapper>
  );
}
