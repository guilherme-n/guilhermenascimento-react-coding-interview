import { IPerson } from '@lib/models/person';
import { useCallback, useEffect, useState } from 'react';

import { contactsClient } from '@lib/clients/contacts';

export function useContactEdit(id: string) {
  const [contact, setContact] = useState({} as IPerson);

  useEffect(() => {
    contactsClient.getContactById(id).then(data => {
      setContact(data);
    });
  }, []);

  const updateContact = (updated: IPerson) => {
    return contactsClient.updateContact(contact.id, updated);
  };

  return {
    contact: contact,
    update: updateContact
  };
}
