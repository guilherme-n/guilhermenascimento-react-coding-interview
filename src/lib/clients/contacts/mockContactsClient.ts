import { faker } from '@faker-js/faker';

import { IPerson } from '@lib/models/person';
import { IContactsClient, IContactListArgs, IContactListResult } from './contactTypes';

function seedContacts(count: number) {
  const res: IPerson[] = [];

  for (let i = 0; i < count; i++) {
    res.push({
      id: faker.datatype.uuid(),
      email: faker.internet.email(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName()
    });
  }

  return res;
}

const delay = (ms: number): Promise<void> => new Promise(res => setTimeout(res, ms));

const CONTACTS_LOCAL_STORAGE_KEY = '@contacts:1-0';

export class MockContactsClient implements IContactsClient {
  private readonly apiContacts: IPerson[];

  constructor(mockPersonsCount: number) {
    const localStorageContacts = localStorage.getItem(CONTACTS_LOCAL_STORAGE_KEY);

    if (localStorageContacts) {
      console.log('local storage');
      this.apiContacts = JSON.parse(localStorageContacts);
    } else {
      console.log('no local storage');
      this.apiContacts = seedContacts(mockPersonsCount);
      localStorage.setItem(CONTACTS_LOCAL_STORAGE_KEY, JSON.stringify(this.apiContacts));
    }
  }

  async contactList(opts: IContactListArgs): Promise<IContactListResult> {
    const { pageNumber = 1, pageSize = 10 } = opts;

    await delay(1000);
    const skip = (pageNumber - 1) * pageSize;
    const take = pageSize;

    return {
      data: this.apiContacts.slice(skip, skip + take),
      totalCount: this.apiContacts.length
    };
  }

  getContactById(id: string): Promise<IPerson> {
    return new Promise(resolve =>
      setTimeout(() => {
        resolve(this.apiContacts.find(c => c.id === id));
      }, 1000)
    );
  }
  updateContact(id: string, update: IPerson): Promise<IPerson> {
    const indexOfContact = this.apiContacts.findIndex(c => c.id === id);
    this.apiContacts[indexOfContact] = { ...update };

    localStorage.setItem(CONTACTS_LOCAL_STORAGE_KEY, JSON.stringify(this.apiContacts));

    return new Promise(resolve => setTimeout(() => resolve(update), 1000));
  }
}
